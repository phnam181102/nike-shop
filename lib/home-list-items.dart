class ListItems {
  ListItems({required this.title, required this.subTitle, required this.buttonTitle, required this.image, required this.color});
  
  final String title;
  final String subTitle;
  final String buttonTitle;
  final String image;
  final String color;
}

final items = <ListItems>[
  ListItems(
    title: 'WHY NOT .6',
    subTitle: 'Behind The Design',
    buttonTitle: 'Explore',
    image: 'https://images.unsplash.com/photo-1637844528447-aee837ccfc7f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1374&q=80',
    color: ""
  ),
  ListItems(
    title: 'AIR JORDAN XXXVII PRM PF',
    subTitle: 'Nike App Early Access',
    buttonTitle: 'Get It First',
    image: 'https://images.unsplash.com/photo-1621880398780-27ebc04d70be?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
    color: ""
  ),
  ListItems(
    title: 'REINVENT YOURSELF WITH JOE HOLDER',
    subTitle: "Trained Podcast",
    buttonTitle: 'Listen Now',
    image: 'https://images.unsplash.com/photo-1637844528447-aee837ccfc7f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1374&q=80',
    color: ""
  ),
];