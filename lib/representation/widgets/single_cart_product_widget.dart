import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:nike_app_clone/controllers/bag_controller.dart';
import 'package:nike_app_clone/controllers/favorites_controller.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';
import 'package:nike_app_clone/models/product.dart';

class SingleCartProduct extends StatefulWidget {
  SingleCartProduct({required this.product, this.quantity});

  final Product product;
  final int? quantity;

  @override
  State<SingleCartProduct> createState() => _SingleCartProductState();
}

class _SingleCartProductState extends State<SingleCartProduct> {
  final FavoritesController favoritesController =
      Get.put(FavoritesController());
  final BagController bagController = Get.put(BagController());

  int quantity = 1;
  bool isChecked = false;
  int price = -1;

  @override
  void initState() {
    super.initState();
    if (widget.quantity != null) {
      quantity = widget.quantity!;
      price = quantity * int.parse(widget.product.price);
    }
  }

  Widget _buildQuentityPart() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          height: 34,
          width: 98,
          decoration: BoxDecoration(
              color: ColorPalette.subColor.withOpacity(0.1),
              borderRadius: BorderRadius.circular(8)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              GestureDetector(
                child: Icon(
                  Icons.remove,
                  color: ColorPalette.darkColor,
                ),
                onTap: () {
                  setState(() {
                    if (quantity > 1) {
                      quantity--;
                      price = quantity * int.parse(widget.product.price);
                      if ( widget.quantity != null) {
                        bagController.updateQuantityInBag(widget.product.id, quantity);
                      }
                    }
                  });
                },
              ),
              Container(
                width: 22,
                child: Text(
                  quantity.toString(),
                  style: TextStyle(fontSize: 16, color: ColorPalette.darkColor),
                  textAlign: TextAlign.center,
                ),
              ),
              GestureDetector(
                child: Icon(
                  Icons.add,
                  color: ColorPalette.darkColor,
                ),
                onTap: () {
                  setState(() {
                    quantity++;
                    price = quantity * int.parse(widget.product.price);
                    if ( widget.quantity != null) {
                        bagController.updateQuantityInBag(widget.product.id, quantity);
                      }
                  });
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 135,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 125,
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              boxShadow: [
                BoxShadow(
                  color: ColorPalette.subColor.withOpacity(0.15),
                  blurRadius: 5.0, // soften the shadow
                  spreadRadius: 0.0, //extend the shadow
                  offset: Offset(
                    3.0, // Move to right 10  horizontally
                    3.0, // Move to bottom 10 Vertically
                  ),
                )
              ],
            ),
            child: Card(
              elevation: 0,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    height: 120,
                    width: 120,
                    child: Stack(fit: StackFit.expand, children: [
                      Positioned(
                        child: Container(
                            decoration: BoxDecoration(
                                borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(8),
                                    bottomLeft: Radius.circular(8)),
                                image: DecorationImage(
                                    image: NetworkImage(
                                        '${widget.product.thumbnail}'),
                                    fit: BoxFit.cover))),
                      ),
                      Positioned(
                        top: 0,
                        left: 0,
                        child: Transform.scale(
                          scale: 0.9,
                          child: Checkbox(
                            value: isChecked,
                            onChanged: (bool? value) {
                              setState(() {
                                isChecked = value!;
                                if (isChecked) {
                                  bagController.addToSelectedProducts(
                                      widget.product.id, quantity, int.parse(widget.product.price));
                                } else {
                                  bagController.removeFromSelectedProducts(
                                      widget.product.id);
                                }
                              });
                            },
                            checkColor: Colors.white, // Màu của icon check
                            activeColor:
                                ColorPalette.darkColor, // Màu nền của checkbox
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(4),
                              side: BorderSide(
                                color: ColorPalette
                                    .darkColor, // Màu viền của checkbox
                                width: 2,
                              ),
                            ),
                          ),
                        ),
                      )
                    ]),
                  ),
                  Container(
                    height: 120,
                    width: 200,
                    padding: EdgeInsets.fromLTRB(16, 8, 0, 8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(widget.product.category,
                            style: TextStyles.defaultStyle.medium
                                .setColor(ColorPalette.darkColor)
                                .setTextSize(12.5),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis),
                        SizedBox(
                          height: 1,
                        ),
                        Container(
                          width: 180,
                          child: Text(widget.product.name,
                              style: TextStyles.defaultStyle.medium
                                  .setColor(ColorPalette.darkColor)
                                  .setTextSize(16),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis),
                        ),
                        SizedBox(
                          height: 6,
                        ),
                        Text("\$${price < 0 ? widget.product.price : price}",
                            style: TextStyles.defaultStyle.medium
                                .setColor(ColorPalette.darkColor)
                                .setTextSize(16)),
                        SizedBox(
                          height: 8,
                        ),
                        _buildQuentityPart()
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 4, right: 10),
                    height: double.infinity,
                    child: GestureDetector(
                        onTap: () {
                          print(widget.quantity);
                          if (widget.quantity != null) {
                            bagController.removeFromBag(widget.product.id);
                          } else {
                            favoritesController
                                .removeFromFavorites(widget.product.id);
                          }
                        },
                        child: SvgPicture.asset("assets/icons/delete.svg")),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
