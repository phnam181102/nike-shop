import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';
import 'package:nike_app_clone/models/product.dart';
import 'package:nike_app_clone/representation/screens/detail_screen.dart';

class SingleProduct extends StatelessWidget {
  SingleProduct({required this.product, this.isLargeSize = false});

  Product product;

  final bool isLargeSize;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (ctx) => DetailScreen(
              title: product.name,
              product: product,
            ),
          ),
        );
      },
      child: Card(
        elevation: 0,
        child: Container(
          height: 260,
          width: isLargeSize ? 160 : 146,
          margin: isLargeSize
              ? EdgeInsets.only(right: 0)
              : EdgeInsets.only(right: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 185,
                width: isLargeSize ? 160 : 146,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xFFE7E7E7),
                    image: DecorationImage(
                        image: NetworkImage('${product.thumbnail}'),
                        fit: BoxFit.cover)),
              ),
              Expanded(
                child: Container(
                  height: double.infinity,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: isLargeSize ? 10 : 0,
                      ),
                      Text(product.category,
                          style: TextStyles.defaultStyle.medium
                              .setColor(ColorPalette.darkColor)
                              .setTextSize(14),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis),
                      SizedBox(
                        height: 5,
                      ),
                      Text(product.name,
                          style: TextStyles.defaultStyle.regular
                              .setColor(ColorPalette.subColor)
                              .setTextSize(14),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis),
                      SizedBox(
                        height: 8,
                      ),
                      Text("\$${product.price}",
                          style: TextStyles.defaultStyle.regular
                              .setColor(ColorPalette.darkColor)
                              .setTextSize(14)),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
