import 'package:flutter/material.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';

class ButtonWidget extends StatelessWidget {
  const ButtonWidget({Key? key, required this.title, required this.isFill, this.onTap, this.isDark = false})
      : super(key: key);

  final String title;
  final bool isFill;
  final bool isDark;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.symmetric(vertical: 15),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            color: isFill ? (isDark ? ColorPalette.darkColor : ColorPalette.whiteColor) : Colors.transparent,
            border: Border.all(color: isFill ? Color.fromARGB(255, 0, 0, 0) : ColorPalette.whiteColor)    
        ),
            
        child: Text(
          title,
          style: isFill ? (isDark ? TextStyles.defaultStyle.medium.setTextSize(17).whiteColor: TextStyles.defaultStyle.medium.setTextSize(17).darkColor) : TextStyles.defaultStyle.medium.setTextSize(17).whiteColor,
        ),
      ),
    );
  }
}