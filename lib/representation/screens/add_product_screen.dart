import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

import 'package:nike_app_clone/models/product.dart';

class AddProductScreen extends StatefulWidget {
  @override
  _AddProductScreenState createState() => _AddProductScreenState();
}

class _AddProductScreenState extends State<AddProductScreen> {
  final _formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final categoryController = TextEditingController();
  final descriptionController = TextEditingController();
  final priceController = TextEditingController();
  File? _thumbnail;
  List<File> _detailImgs = [];

  Future<void> _pickThumbnail() async {
    final pickedFile =
        await ImagePicker().getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        _thumbnail = File(pickedFile.path);
      });
    }
  }

  Future<void> _pickDetailImgs() async {
    final picker = ImagePicker();
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      _detailImgs.add(File(pickedFile!.path));
    });
  }

  Future<void> _addProductToFirebase() async {
    try {
      if (_formKey.currentState!.validate()) {
        // Generate a new ID for the product
        final String productId =
            FirebaseFirestore.instance.collection('products').doc().id;

        // Upload the thumbnail to Firebase Storage
        final Reference thumbnailRef =
            FirebaseStorage.instance.ref('productThumbnails/$productId');
        final UploadTask thumbnailUploadTask =
            thumbnailRef.putFile(_thumbnail!);
        final TaskSnapshot thumbnailSnapshot =
            await thumbnailUploadTask.whenComplete(() => null);
        final String thumbnailUrl =
            await thumbnailSnapshot.ref.getDownloadURL();

        // Upload the detail images to Firebase Storage
        List<String> detailImgUrls = [];
        for (File detailImg in _detailImgs) {
          final Reference detailImgRef = FirebaseStorage.instance
              .ref('detailImages/$productId')
              .child(detailImg.path.split('/').last);
          final UploadTask detailImgUploadTask =
              detailImgRef.putFile(detailImg);
          final TaskSnapshot detailImgSnapshot =
              await detailImgUploadTask.whenComplete(() => null);
          final String detailImgUrl =
              await detailImgSnapshot.ref.getDownloadURL();
          detailImgUrls.add(detailImgUrl);
        }

        // Create a new product object and add it to Firebase Firestore
        final newProduct = Product(
          name: nameController.text,
          category: categoryController.text,
          description: descriptionController.text,
          thumbnail: thumbnailUrl,
          detailImgs: detailImgUrls,
          price: priceController.text,
          id: productId,
        );
        await FirebaseFirestore.instance
            .collection('products')
            .doc(productId)
            .set(newProduct.toJson());

        Get.snackbar(
          'Success',
          'Đã thêm sản phẩm thành công',
          icon: SvgPicture.asset("assets/icons/success.svg"),
          margin: EdgeInsets.all(15),
          snackPosition: SnackPosition.BOTTOM,
        );
      }
    } catch (e) {
      Get.snackbar(
        'Error',
        e.toString(),
        icon: SvgPicture.asset("assets/icons/error.svg"),
        margin: EdgeInsets.all(15),
        snackPosition: SnackPosition.BOTTOM,
      );
    }
  }

  void _clearAll() {
    setState(() {
      nameController.clear();
      categoryController.clear();
      descriptionController.clear();
      priceController.clear();
      _thumbnail = null;
      _detailImgs.clear();
    });
  }

  Widget _buildDetailImgsPicker() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Detail Images', style: Theme.of(context).textTheme.subtitle1),
        SizedBox(height: 8.0),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ElevatedButton.icon(
              icon: Icon(Icons.photo_library),
              label: Text('Pick image'),
              onPressed: _pickDetailImgs,
            ),
            SizedBox(width: 16.0),
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Wrap(
                  spacing: 8.0,
                  children: _detailImgs.map((img) {
                    return Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Image.file(img, height: 50.0),
                    );
                  }).toList(),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Thêm sản phẩm mới'),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(
                    labelText: 'Name',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Vui lòng nhập tên sản phẩm';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  controller: categoryController,
                  decoration: InputDecoration(
                    labelText: 'Category',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Vui lòng nhập danh mục';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                TextFormField(
                  controller: descriptionController,
                  decoration: InputDecoration(
                    labelText: 'Description',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Vui lòng nhập mô tả';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 16.0),
                Row(
                  children: [
                    Expanded(
                      child: ElevatedButton.icon(
                        icon: Icon(Icons.photo),
                        label: Text('Pick product thumbnail'),
                        onPressed: _pickThumbnail,
                      ),
                    ),
                    SizedBox(width: 16.0),
                    _thumbnail == null
                        ? Container()
                        : Image.file(_thumbnail!, height: 50.0),
                  ],
                ),
                SizedBox(height: 16.0),
                _buildDetailImgsPicker(),
                SizedBox(height: 16.0),
                TextFormField(
                  controller: priceController,
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                    labelText: 'Price',
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Vui lòng nhập mô tả';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 25.0),
                ElevatedButton(
                  child: Text('Save Product'),
                  onPressed: _addProductToFirebase,
                ),
                SizedBox(height: 8.0),
                ElevatedButton(
                  child: Text('Clear All'),
                  onPressed: _clearAll,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
