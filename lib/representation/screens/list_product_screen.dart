import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';
import 'package:nike_app_clone/models/product.dart';
import 'package:nike_app_clone/representation/screens/main_app.dart';
import 'package:nike_app_clone/representation/screens/shop_screen.dart';
import 'package:nike_app_clone/representation/widgets/single_product_widget.dart';

class ListProductScreen extends StatelessWidget {
  ListProductScreen({required this.name, required this.productList});
  List<Product> productList;

  final String name;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: SvgPicture.asset("assets/icons/left-arrow.svg"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          title: Text(name,
              style: TextStyles.defaultStyle.medium
                  .setColor(ColorPalette.darkColor)
                  .setTextSize(18)),
          actions: <Widget>[
            IconButton(
                onPressed: () {},
                icon: SvgPicture.asset("assets/icons/filter.svg")),
            IconButton(
                onPressed: () {},
                icon: SvgPicture.asset(
                  "assets/icons/search.svg",
                  color: ColorPalette.darkColor,
                  height: 21,
                  width: 21,
                )),
          ],
        ),
        body: Container(
            child: ListView(
          children: [
            Column(
              children: <Widget>[
                Container(
                  child: Column(children: [
                    SizedBox(
                      height: 12,
                    ),
                    Container(
                      height: 700,
                      child: GridView.count(
                          padding: EdgeInsets.symmetric(horizontal: 12),
                          mainAxisSpacing: 20,
                          crossAxisSpacing: 14,
                          childAspectRatio: 0.65,
                          crossAxisCount: 2,
                          children: productList
                              .map((product) => SingleProduct(
                                    product: product,
                                    isLargeSize: true,
                                  ))
                              .toList()),
                    )
                  ]),
                )
              ],
            ),
          ],
        )));
  }
}
