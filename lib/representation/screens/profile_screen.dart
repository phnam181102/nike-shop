import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:nike_app_clone/constants.dart';
import 'package:nike_app_clone/controllers/profile_controller.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';
import 'package:nike_app_clone/representation/screens/edit_profile_screen.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  static String routeName = '/profile_screen';
  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final ProfileController profileController = Get.put(ProfileController());

  @override
  void initState() {
    super.initState();
    profileController.getUserData();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProfileController>(
        init: ProfileController(),
        builder: (controller) {
          if (controller.user.isEmpty) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          return Obx(() {
            if (controller.user.isEmpty) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            return Scaffold(
                backgroundColor: Color(0xFFEAEAEA),
                body: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 15),
                          color: ColorPalette.whiteColor,
                          width: double.infinity,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  height: 42,
                                ),
                                SizedBox(
                                  height: 90,
                                  child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 28.0),
                                        child: Obx(
                                          () => CircleAvatar(
                                            backgroundImage: (controller
                                                        .pickedImage !=
                                                    null)
                                                ? FileImage(
                                                    controller.pickedImage!)
                                                : NetworkImage(
                                                        '${controller.user['profilePhoto']}')
                                                    as ImageProvider<Object>?,
                                            backgroundColor: Colors.grey[350],
                                            radius: 42,
                                          ),
                                        ),
                                      ),
                                   
                                ),
                                SizedBox(
                                  height: 26,
                                ),
                                Text(
                                    '${controller.user['firstName']} ${controller.user['lastName']}'
                                        .toUpperCase(),
                                    style: TextStyles.defaultStyle.bold
                                        .setColor(ColorPalette.darkColor)
                                        .setTextSize(16)),
                                SizedBox(
                                  height: 36,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).push(
                                      MaterialPageRoute(
                                        builder: (ctx) =>
                                            const EditProfileScreen(),
                                      ),
                                    );
                                  },
                                  child: Container(
                                    width: 170,
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.symmetric(vertical: 8),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.transparent,
                                        border: Border.all(
                                            color: Color.fromARGB(
                                                255, 163, 163, 163),
                                            width: 1)),
                                    child: Text(
                                      'EDIT PROFILE',
                                      style: TextStyles.defaultStyle.regular
                                          .setTextSize(13)
                                          .darkColor,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 40,
                                ),
                                IntrinsicHeight(
                                    child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          SvgPicture.asset(
                                              "assets/icons/ordersminor.svg"),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text("Orders",
                                              style: TextStyles
                                                  .defaultStyle.medium
                                                  .setTextSize(11)
                                                  .darkColor),
                                        ]),
                                    Container(
                                      height: 28,
                                      child: VerticalDivider(
                                        thickness: 1,
                                        color:
                                            Color.fromARGB(255, 185, 185, 185),
                                      ),
                                    ),
                                    Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          SvgPicture.asset(
                                              "assets/icons/pass.svg"),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text("Pass",
                                              style: TextStyles
                                                  .defaultStyle.medium
                                                  .setTextSize(11)
                                                  .darkColor),
                                        ]),
                                    Container(
                                      height: 28,
                                      child: VerticalDivider(
                                        thickness: 1,
                                        color:
                                            Color.fromARGB(255, 204, 204, 204),
                                      ),
                                    ),
                                    Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          SvgPicture.asset(
                                              "assets/icons/event.svg"),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          Text("Events",
                                              style: TextStyles
                                                  .defaultStyle.medium
                                                  .setTextSize(11)
                                                  .darkColor),
                                        ]),
                                    Container(
                                      height: 28,
                                      child: VerticalDivider(
                                        thickness: 1,
                                        color:
                                            Color.fromARGB(255, 185, 185, 185),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        authController.logOut();
                                      },
                                      child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          children: [
                                            SvgPicture.asset(
                                                "assets/icons/log-out.svg"),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text("Log Out",
                                                style: TextStyles
                                                    .defaultStyle.semibold
                                                    .setTextSize(11)
                                                    .setColor(
                                                        Color(0xFFEF7952))),
                                          ]),
                                    ),
                                  ],
                                )),
                                SizedBox(
                                  height: 12,
                                ),
                              ]),
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        IntrinsicHeight(
                            child: Container(
                                color: ColorPalette.whiteColor,
                                padding: EdgeInsets.symmetric(horizontal: 12),
                                width: double.infinity,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    GestureDetector(
                                        onTap: () {
                                          // Navigator.push(
                                          //   context,
                                          //   MaterialPageRoute(
                                          //     builder: (context) => NotificationScreen(notifications: notifications),
                                          //   ),
                                          // );
                                        },
                                        child: Container(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 28, 0, 26),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text('INBOX',
                                                      style: TextStyles
                                                          .defaultStyle.semibold
                                                          .setColor(ColorPalette
                                                              .darkColor)
                                                          .setTextSize(14)),
                                                  SizedBox(
                                                    height: 5,
                                                  ),
                                                  Text('View messages',
                                                      style: TextStyles
                                                          .defaultStyle.regular
                                                          .setColor(ColorPalette
                                                              .subColor)
                                                          .setTextSize(11))
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  // Container(
                                                  //   alignment: Alignment.center,
                                                  //   height: 26,
                                                  //   width: 26,
                                                  //   // padding: EdgeInsets.all(4),
                                                  //   decoration: BoxDecoration(
                                                  //     color: Color.fromARGB(
                                                  //         255, 231, 84, 26),
                                                  //     borderRadius:
                                                  //         BorderRadius.circular(
                                                  //             13),
                                                  //   ),
                                                  //   child: Text("3",
                                                  //       style: TextStyles
                                                  //           .defaultStyle.medium
                                                  //           .setColor(
                                                  //               ColorPalette
                                                  //                   .whiteColor)
                                                  //           .setTextSize(14)),
                                                  // ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  SvgPicture.asset(
                                                      "assets/icons/right-arrow.svg")
                                                ],
                                              )
                                            ],
                                          ),
                                        )),
                                    Divider(
                                        thickness: 0.5,
                                        color:
                                            Color.fromARGB(255, 201, 201, 201)),
                                    Container(
                                      padding:
                                          EdgeInsets.fromLTRB(0, 26, 0, 28),
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text('YOUR NIKE MEMBER REWARDS',
                                                  style: TextStyles
                                                      .defaultStyle.semibold
                                                      .setColor(ColorPalette
                                                          .darkColor)
                                                      .setTextSize(14)),
                                              SizedBox(
                                                height: 5,
                                              ),
                                              Text('2 available',
                                                  style: TextStyles
                                                      .defaultStyle.regular
                                                      .setColor(
                                                          ColorPalette.subColor)
                                                      .setTextSize(11))
                                            ],
                                          ),
                                          SvgPicture.asset(
                                              "assets/icons/right-arrow.svg")
                                        ],
                                      ),
                                    )
                                  ],
                                )))
                      ],
                    ),
                    Container(
                        margin: EdgeInsets.only(bottom: 10),
                        child: Text(
                            'Member Since ${controller.user['createdDate']}',
                            style: TextStyles.defaultStyle.regular
                                .setColor(ColorPalette.subColor)
                                .setTextSize(13))),
                  ],
                ));
          });
        });
  }
}
