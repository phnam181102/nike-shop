import 'package:animated_fractionally_sized_box/animated_fractionally_sized_box.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:nike_app_clone/controllers/favorites_controller.dart';

import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';
import 'package:nike_app_clone/models/product.dart';
import 'package:nike_app_clone/representation/screens/main_app.dart';
import 'package:nike_app_clone/representation/widgets/button_widget.dart';
import 'package:shimmer/shimmer.dart';

import '../../constants.dart';
import '../../controllers/bag_controller.dart';
import '../../controllers/review_controller.dart';

class DetailScreen extends StatefulWidget {
  DetailScreen({required this.title, required this.product});

  Product product;

  final String title;

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  int _currentIndex = 0;
  final CarouselController _controller = CarouselController();
  final FavoritesController favoritesController =
      Get.put(FavoritesController());
  final BagController bagController = Get.put(BagController());
  final ReviewController reviewController = Get.put(ReviewController());

  final TextEditingController textController = TextEditingController();

  @override
  void dispose() {
    reviewController.clearComments();
    super.dispose();
  }

  @override
  void initState() {
    reviewController.getComments(widget.product.id);
    super.initState();
  }

  int _rating = 0;
  String _comment = '';

  void clearComment() {
    setState(() {
      _rating = 0;
      textController.clear();
    });
  }

  bool _isSubmitEnabled() {
    return _rating > 0 && _comment.isNotEmpty;
  }

  @override
  Widget build(BuildContext context) {
    int _totalSlides = widget.product.detailImgs.length;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: SvgPicture.asset("assets/icons/left-arrow.svg"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        title: Text(widget.title,
            style: TextStyles.defaultStyle.medium
                .setColor(ColorPalette.darkColor)
                .setTextSize(18)),
      ),
      body: SingleChildScrollView(
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Container(
              width: double.infinity,
              child: Column(
                children: [
                  CarouselSlider(
                    carouselController: _controller,
                    options: CarouselOptions(
                      height: 420,
                      viewportFraction: 1.0,
                      enableInfiniteScroll: false,
                      onPageChanged: (index, reason) {
                        setState(() {
                          _currentIndex = index;
                        });
                      },
                    ),
                    items: widget.product.detailImgs.map((imgUrl) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: NetworkImage(imgUrl),
                                fit: BoxFit.cover,
                              ),
                            ),
                          );
                        },
                      );
                    }).toList(),
                  ),
                  Container(
                    height: 2.0,
                    color: Colors.grey,
                    child: Stack(
                      children: [
                        Container(
                          child: AnimatedFractionallySizedBox(
                            duration: Duration(milliseconds: 250),
                            alignment: Alignment.centerLeft,
                            widthFactor: (_currentIndex + 1) / _totalSlides,
                            child: Container(
                              color: Colors.black,
                            ),
                          ),
                        ),
                        FractionallySizedBox(
                          alignment: Alignment.centerLeft,
                          widthFactor: 1,
                          child: Container(
                            color: Colors.transparent,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(20, 38, 20, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.product.category,
                          style: TextStyles.defaultStyle.medium
                              .setColor(ColorPalette.darkColor)
                              .setTextSize(16),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          widget.product.name,
                          style: TextStyles.defaultStyle.semibold
                              .setColor(ColorPalette.darkColor)
                              .setTextSize(24),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          "\$${widget.product.price}",
                          style: TextStyles.defaultStyle.medium
                              .setColor(ColorPalette.darkColor)
                              .setTextSize(24),
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        Text(
                          widget.product.description,
                          style: TextStyles.defaultStyle.regular
                              .setColor(ColorPalette.darkColor)
                              .setTextSize(15)
                              .setTextHeight(1.65),
                        ),
                        SizedBox(
                          height: 32,
                        ),
                        GestureDetector(
                          onTap: () {
                            bagController.addToSelectedProducts(
                                widget.product.id,
                                1,
                                int.parse(widget.product.price));
                            bagController.addToBag();
                          },
                          child: Container(
                            child: ButtonWidget(
                              isFill: true,
                              title: "Add to Bag",
                              isDark: true,
                            ),
                          ),
                        ),
                        SizedBox(height: 12),
                        GestureDetector(
                          onTap: () {
                            favoritesController
                                .addToFavorites(widget.product.id);
                          },
                          child: Container(
                            child: ButtonWidget(
                              isFill: true,
                              title: "Favorite",
                            ),
                          ),
                        ),
                        SizedBox(height: 50),
                        Divider(
                            thickness: 2,
                            color: Color.fromARGB(180, 201, 201, 201)),
                        SizedBox(height: 20),
                        Container(
                          child: Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.all(10),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Write a Review",
                                        style: TextStyles.defaultStyle.semibold
                                            .setColor(ColorPalette.darkColor)
                                            .setTextSize(20)),
                                    Row(
                                      children: [
                                        for (int i = 1; i <= 5; i++)
                                          GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                _rating = i;
                                              });
                                            },
                                            child: Container(
                                              margin: EdgeInsets.only(right: 3),
                                              child: SvgPicture.asset(
                                                "assets/icons/star.svg",
                                                width: 18,
                                                height: 18,
                                                color: i <= _rating
                                                    ? Colors.amber
                                                    : Colors.grey[300],
                                              ),
                                            ),
                                          ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: TextFormField(
                                      decoration: InputDecoration(
                                          hintText:
                                              "Would you like to write anything about this product?",
                                          contentPadding: EdgeInsets.all(20),
                                          filled: true,
                                          fillColor:
                                              Colors.black.withOpacity(0.05),
                                          border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(14),
                                            borderSide: BorderSide.none,
                                          )),
                                      onChanged: (value) => {
                                        setState(() {
                                          _comment = value;
                                        })
                                      },
                                      controller: textController,
                                      validator: (value) {
                                        if (value!.isEmpty) {
                                          return ("Please enter a valid email address.");
                                        }
                                        if (!RegExp(
                                                "^[a-zA-Z0-9+_.-]+@[a-z0-9A-Z.-]+.[a-z]")
                                            .hasMatch(value)) {
                                          return ("Please enter a valid email address.");
                                        }
                                        return null;
                                      },
                                      onSaved: (value) {
                                        textController.text = value!;
                                      },
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  ElevatedButton(
                                    onPressed: _isSubmitEnabled()
                                        ? () {
                                            reviewController
                                                .saveCommentToFirestore(
                                              widget.product.id,
                                              _rating,
                                              _comment,
                                            );
                                            clearComment();
                                          }
                                        : null,
                                    style: ElevatedButton.styleFrom(
                                      primary: ColorPalette.darkColor,
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                      ),
                                    ),
                                    child: SizedBox(
                                      width: 50,
                                      height: 56,
                                      child: Center(
                                        child: Text('Send',
                                            style: TextStyles
                                                .defaultStyle.semibold
                                                .setColor(
                                                    ColorPalette.whiteColor)
                                                .setTextSize(17)),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 50,
                        ),
                        Obx(() => Column(
                              children: [
                                ListView.separated(
                                  shrinkWrap: true,
                                  itemCount: reviewController.comments.length,
                                  separatorBuilder: (context, index) =>
                                      SizedBox(height: 15),
                                  itemBuilder: (context, index) {
                                    final comment =
                                        reviewController.comments[index];
                                    final user = reviewController
                                        .getUserData(comment.userId);
                                    return FutureBuilder<Map<String, dynamic>>(
                                      future: user,
                                      builder: (context, snapshot) {
                                        if (snapshot.connectionState ==
                                            ConnectionState.waiting) {
                                          return Shimmer.fromColors(
                                            baseColor: ColorPalette.subColor,
                                            highlightColor:
                                                ColorPalette.subColor,
                                            child: Container(
                                              height: 80,
                                              color: Colors.white,
                                            ),
                                          );
                                        }
                                        if (snapshot.hasError) {
                                          return Text(
                                              "Error loading user data");
                                        }
                                        final userData = snapshot.data;
                                        if (userData != null) {
                                          final firstName =
                                              userData['firstName'];
                                          final lastName = userData['lastName'];
                                          return Container(
                                            margin: EdgeInsets.only(bottom: 5),
                                            decoration: new BoxDecoration(
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey
                                                      .withOpacity(.25),
                                                  blurRadius:
                                                      8.0, // soften the shadow
                                                  spreadRadius:
                                                      0.0, //extend the shadow
                                                  offset: Offset(
                                                    3.0, // Move to right 10  horizontally
                                                    3.0, // Move to bottom 10 Vertically
                                                  ),
                                                )
                                              ],
                                            ),
                                            child: Padding(
                                              padding: EdgeInsets.all(16),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Row(children: [
                                                        CircleAvatar(
                                                          backgroundImage:
                                                              NetworkImage(
                                                                      '${userData['profilePhoto']}')
                                                                  as ImageProvider<
                                                                      Object>?,
                                                          backgroundColor:
                                                              Colors.grey[350],
                                                          radius: 18,
                                                        ),
                                                        SizedBox(width: 10),
                                                        Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                                "$firstName $lastName",
                                                                style: TextStyles
                                                                    .defaultStyle
                                                                    .medium
                                                                    .setColor(Color
                                                                        .fromARGB(
                                                                            255,
                                                                            50,
                                                                            50,
                                                                            50))
                                                                    .setTextSize(
                                                                        17)),
                                                            SizedBox(
                                                              height: 4,
                                                            ),
                                                            Text(
                                                                "${DateFormat('MMMM dd, yyyy').format(comment.timestamp)}",
                                                                style: TextStyles
                                                                    .defaultStyle
                                                                    .setColor(
                                                                        ColorPalette
                                                                            .subColor)
                                                                    .setTextSize(
                                                                        12)),
                                                          ],
                                                        ),
                                                      ]),
                                                      Row(
                                                        children: List.generate(
                                                          5,
                                                          (index) => Container(
                                                            margin:
                                                                EdgeInsets.only(
                                                                    right: 3),
                                                            child: SvgPicture
                                                                .asset(
                                                              "assets/icons/star.svg",
                                                              width: 15,
                                                              height: 15,
                                                              color: index <
                                                                      comment
                                                                          .rating
                                                                  ? Colors.amber
                                                                  : Colors.grey[
                                                                      300],
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  comment.content.isEmpty
                                                      ? SizedBox(height: 0)
                                                      : SizedBox(height: 15),
                                                  Text(comment.content,
                                                      style: TextStyles
                                                          .defaultStyle
                                                          .setColor(
                                                              Color.fromARGB(
                                                                  255,
                                                                  59,
                                                                  59,
                                                                  59))
                                                          .setTextHeight(1.2)
                                                          .setTextSize(15)),
                                                ],
                                              ),
                                            ),
                                          );
                                        } else {
                                          return Text(
                                              "User not found"); // Hiển thị thông báo nếu không tìm thấy thông tin người dùng
                                        }
                                      },
                                    );
                                  },
                                ),
                                SizedBox(
                                  height: 40,
                                )
                              ],
                            ))
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
