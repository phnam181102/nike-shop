import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:nike_app_clone/constants.dart';
import 'package:nike_app_clone/controllers/favorites_controller.dart';
import 'package:nike_app_clone/controllers/profile_controller.dart';
import 'package:nike_app_clone/controllers/search_controller.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';
import 'package:nike_app_clone/representation/screens/list_product_screen.dart';

import '../widgets/single_product_widget.dart';

class ShopScreen extends StatefulWidget {
  const ShopScreen({Key? key}) : super(key: key);

  static String routeName = "/shop_screen";

  @override
  State<ShopScreen> createState() => _ShopScreenState();
}

class _ShopScreenState extends State<ShopScreen> {
  final ProfileController profileController = Get.put(ProfileController());
  final FavoritesController favoritesController =
      Get.put(FavoritesController());
  final SearchController searchController = Get.put(SearchController());

  @override
  void initState() {
    super.initState();
    profileController.getUserData();
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProfileController>(
      init: ProfileController(),
      builder: (controller) {
        if (controller.user.isEmpty && favoritesController.productList.isEmpty) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        return Obx(() {
          return Scaffold(
            body: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.topLeft,
                      child: Text(
                        'Good Evening, ${controller.user['firstName']}',
                        style: TextStyles.defaultStyle.medium
                            .setColor(ColorPalette.darkColor)
                            .setTextSize(25),
                      ),
                    ),
                    SizedBox(
                      height: 17,
                    ),
                    Container(
                      alignment: Alignment.center,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Color(0xFFF3F3F3),
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: TextFormField(
                        cursorColor: ColorPalette.darkColor,
                        decoration: InputDecoration(
                          prefixIcon: Container(
                            padding: EdgeInsets.only(left: 15, right: 10),
                            child: SvgPicture.asset("assets/icons/search.svg"),
                          ),
                          contentPadding: const EdgeInsets.only(left: 14.0, top: 14.0),
                          hintText: "Search Product",
                          border: InputBorder.none,
                          suffixIcon: Container(
                            margin: EdgeInsets.fromLTRB(0, 12, 0, 12),
                            padding: EdgeInsets.only(left: 15, right: 15),
                            decoration: BoxDecoration(
                              border: Border(
                                left: BorderSide(
                                  width: 1,
                                  color: Color(0xFF878787).withOpacity(0.3),
                                ),
                              ),
                            ),
                            child: Icon(Icons.mic, color: Color(0xFF878787)),
                          ),
                        ),
                        onChanged: (value) {
                          searchController.searchProduct(value);
                        },
                      ),
                    ),
                    SizedBox(
                      height: 28,
                    ),
                    searchController.typeProduct.isNotEmpty
                        ? (searchController.searchedProducts.isEmpty
                            ? Text("Sorry, I couldn't find any matching products.")
                            : Column(
                                children: [
                                  Container(
                                    height: 700,
                                    child: GridView.count(
                                      padding: EdgeInsets.only(bottom: 150),
                                      mainAxisSpacing: 20,
                                      crossAxisSpacing: 14,
                                      childAspectRatio: 0.65,
                                      crossAxisCount: 2,
                                      children: searchController.searchedProducts.map(
                                        (product) => SingleProduct(
                                          product: product,
                                          isLargeSize: true,
                                        ),
                                      ).toList(),
                                    ),
                                  ),
                                ],
                              )
                          )
                        : Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    "Top Picks For You",
                                    style: TextStyles.defaultStyle.medium
                                        .setColor(ColorPalette.darkColor)
                                        .setTextSize(18),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (ctx) => ListProductScreen(
                                            name: "Top Picks For You",
                                            productList: favoritesController.productList,
                                          ),
                                        ),
                                      );
                                    },
                                    child: Text(
                                      "View All",
                                      style: TextStyles.defaultStyle.regular
                                          .setColor(ColorPalette.subColor)
                                          .setTextSize(14),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 12),
                              Container(
                                height: 270.0,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: 4,
                                  itemBuilder: (BuildContext context, int index) {
                                    final product = favoritesController.productList[index];
                                    return SingleProduct(
                                      product: product,
                                    );
                                  },
                                ),
                              ),
                               SizedBox(
                                height: 36,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    "New Arrivals",
                                    style: TextStyles.defaultStyle.medium
                                        .setColor(ColorPalette.darkColor)
                                        .setTextSize(18),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (ctx) => ListProductScreen(
                                            name: "New Arrivals",
                                            productList: favoritesController.productList,
                                          ),
                                        ),
                                      );
                                    },
                                    child: Text(
                                      "View All",
                                      style: TextStyles.defaultStyle.regular
                                          .setColor(ColorPalette.subColor)
                                          .setTextSize(14),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 12),
                              Container(
                                height: 270.0,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: 4,
                                  itemBuilder: (BuildContext context, int index) {
                                    final product = favoritesController.productList[index + 4];
                                    return SingleProduct(
                                      product: product,
                                    );
                                  },
                                ),
                              ),
                              SizedBox(
                                height: 36,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    "This Week's Top Picks",
                                    style: TextStyles.defaultStyle.medium
                                        .setColor(ColorPalette.darkColor)
                                        .setTextSize(18),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (ctx) => ListProductScreen(
                                            name: "This Week's Top Picks",
                                            productList: favoritesController.productList,
                                          ),
                                        ),
                                      );
                                    },
                                    child: Text(
                                      "View All",
                                      style: TextStyles.defaultStyle.regular
                                          .setColor(ColorPalette.subColor)
                                          .setTextSize(14),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 12),
                              Container(
                                height: 270.0,
                                child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: 4,
                                  itemBuilder: (BuildContext context, int index) {
                                    final product = favoritesController.productList[index + 8];
                                    return SingleProduct(
                                      product: product,
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                  ],
                ),
              ),
            ),
          );
        });
      },
    );
  }
}