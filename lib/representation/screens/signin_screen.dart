import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nike_app_clone/constants.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';
import 'package:nike_app_clone/representation/screens/forgot_password_screen.dart';
import 'package:nike_app_clone/representation/screens/intro_screen.dart';
import 'package:nike_app_clone/representation/screens/main_app.dart';
import 'package:nike_app_clone/representation/screens/signup_screen.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  static String routeName = '/signin_screen';

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();


  final TextEditingController emailController = TextEditingController();

  final TextEditingController passwordController = TextEditingController();

  bool _passwordVisible = false;

  String email = '';
  String password = '';

  void validation() async {
    if (_formKey.currentState!.validate()) {
      authController.loginUser(emailController.text, passwordController.text);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.fromLTRB(0, 120, 0, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 20),
                child: Image.asset('assets/images/nike-sm-logo-black.png',
                    width: 53, height: 19),
              ),
              Padding(
                  padding: EdgeInsets.only(left: 20, top: 28),
                  child: SizedBox(
                    child: Text(
                      "Your account for Everything Nike",
                      style: TextStyles.defaultStyle.medium
                          .setColor(ColorPalette.darkColor)
                          .setTextSize(28),
                    ),
                  )),
              Padding(
                padding: EdgeInsets.only(left: 20, top: 44, right: 20),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        decoration: InputDecoration(
                            hintText: "Email address",
                            contentPadding: EdgeInsets.all(20),
                            filled: true,
                            fillColor: Colors.black.withOpacity(0.05),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(14),
                              borderSide: BorderSide.none,
                            )),
                        onChanged: (value) => {
                          setState(() {
                            email = value;
                          })
                        },
                        controller: emailController,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return ("Please enter a valid email address.");
                          }
                          if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-z0-9A-Z.-]+.[a-z]")
                              .hasMatch(value)) {
                            return ("Please enter a valid email address.");
                          }
                          return null;
                        },
                        onSaved: (value) {
                          emailController.text = value!;
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            hintText: "Password",
                            contentPadding: EdgeInsets.all(20),
                            suffixIcon: IconButton(
                              icon: Icon(
                                // Based on passwordVisible state choose the icon
                                _passwordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: Theme.of(context).primaryColorDark,
                              ),
                              onPressed: () {
                                // Update the state i.e. toogle the state of passwordVisible variable
                                setState(() {
                                  _passwordVisible = !_passwordVisible;
                                });
                              },
                            ),
                            filled: true,
                            fillColor: Colors.black.withOpacity(0.05),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(14),
                              borderSide: BorderSide.none,
                            )),
                        onChanged: (value) => {
                          setState(() {
                            password = value;
                          })
                        },
                        controller: passwordController,
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          RegExp regex = RegExp(r'^.{8,}$');
                          if (value!.isEmpty) {
                            return ("Please enter a password.");
                          }
                          if (!regex.hasMatch(value)) {
                            return ("Please enter a valid password.");
                          }
                        },
                        onSaved: (value) {
                          passwordController.text = value!;
                        },
                        obscureText: !_passwordVisible,
                      ),
                    ],
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 16, right: 20),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed(ForgotPasswordScreen.routeName);
                      },
                      child: Text(
                        "Forgotten your password?",
                        style: TextStyles.defaultStyle
                            .setColor(ColorPalette.subColor)
                            .setTextSize(15),
                      ),
                    ),
                  )
                ],
              ),
              Padding(
                padding: EdgeInsets.only(left: 20, top: 40, right: 20),
                child: Column(
                  children: [
                    ElevatedButton(
                      onPressed: () => validation(),
                      style: ElevatedButton.styleFrom(
                        primary: ColorPalette.darkColor,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100.0),
                        ),
                      ),
                      child: SizedBox(
                        width: 335,
                        height: 56,
                        child: Center(
                          child: Text('Sign In',
                              style: TextStyles.defaultStyle.semibold
                                  .setColor(ColorPalette.whiteColor)
                                  .setTextSize(17)),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Don't have an account?",
                          style: TextStyles.defaultStyle.medium
                              .setColor(ColorPalette.subColor)
                              .setTextSize(15),
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .pushNamed(SignUpScreen.routeName);
                          },
                          child: Text("Join Us",
                              style: TextStyles.defaultStyle.medium
                                  .setColor(ColorPalette.darkColor)
                                  .setTextSize(15)),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
