
import 'package:flutter/material.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';
import 'package:nike_app_clone/representation/screens/signin_screen.dart';
import 'package:nike_app_clone/representation/screens/signup_screen.dart';
import 'package:nike_app_clone/representation/widgets/button_widget.dart';

class IntroScreen extends StatefulWidget {
  const IntroScreen({Key? key}) : super(key: key);

  static String routeName = "/intro_screen";

  @override
  State<IntroScreen> createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: Image.asset('assets/images/intro-background.png',
            fit: BoxFit.fitHeight
          )
        ),
        Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                Color.fromARGB(100, 0, 0, 0),
                Color.fromARGB(255, 0, 0, 0),
              ],
            )
          ),
        ),
        Padding(padding: EdgeInsets.fromLTRB(15, 0, 15, 85),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset('assets/images/nike-sm-logo.png', width: 78, height: 28),
            SizedBox(
              height: 48,
            ),
            Text('Nike App', style: TextStyles.defaultStyle.semibold.whiteColor.setTextSize(28)),
            SizedBox(
              height: 8,
            ),
            Text("Bringing Nike Members\nthe best products,\ninspiration and stories\nin sport.", style: TextStyles.defaultStyle.semibold.whiteColor.setTextSize(28)),
            SizedBox(
              height: 33,
            ),
             Container(
                child: Row(
                  children: [
                    Expanded(child: ButtonWidget(title: 'Join Us', isFill: true,  onTap: () {
                          Navigator.of(context).pushNamed(SignUpScreen.routeName);
                        },)),
                    SizedBox(width: 13,),
                    Expanded(child: ButtonWidget(title: 'Sign In', isFill: false, onTap: () {
                          Navigator.of(context).pushNamed(SignInScreen.routeName);
                        },)),
                  ],
                ),
              ),
          ],
        ),)
      ],
    );
  }
}