import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import 'package:nike_app_clone/constants.dart';
import 'package:nike_app_clone/controllers/auth_controller.dart';
import 'package:nike_app_clone/controllers/bag_controller.dart';
import 'package:nike_app_clone/controllers/favorites_controller.dart';
import 'package:nike_app_clone/controllers/profile_controller.dart';

import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/representation/screens/bag_screen.dart';
import 'package:nike_app_clone/representation/screens/favorite_screen.dart';
import 'package:nike_app_clone/representation/screens/home_screen.dart';
import 'package:nike_app_clone/representation/screens/profile_screen.dart';
import 'package:nike_app_clone/representation/screens/shop_screen.dart';

class MainApp extends StatefulWidget {
  const MainApp({Key? key}) : super(key: key);

  static const routeName = '/main_app';

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  final FavoritesController favoritesController =
      Get.put(FavoritesController());
  final BagController bagController = Get.put(BagController());
  final ProfileController profileController = Get.put(ProfileController());


  static List<Widget> _widgetOptions = <Widget>[
    HomeScreen(),
    ShopScreen(),
    FavoriteScreen(),
    BagScreen(),
    ProfileScreen()
  ];

  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    favoritesController.getFavoriteProducts();
    bagController.getBagProducts();
    profileController.getUserData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorPalette.whiteColor,
      body: Container(
          padding: EdgeInsets.only(top: 45),
          child: Center(
            child: _widgetOptions.elementAt(_selectedIndex),
          )),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(top: 2, bottom: 8),
        decoration: BoxDecoration(
            border: Border(
                top: BorderSide(color: Color.fromARGB(25, 116, 116, 116)))),
        child: BottomNavigationBar(
          backgroundColor: ColorPalette.whiteColor,
          selectedItemColor: ColorPalette.darkColor,
          unselectedItemColor: ColorPalette.subColor,
          selectedFontSize: 11,
          unselectedFontSize: 11,
          elevation: 0,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: SvgPicture.asset("assets/icons/home.svg"),
              ),
              activeIcon: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: SvgPicture.asset(
                  "assets/icons/home.svg",
                  color: ColorPalette.darkColor,
                ),
              ),
              label: "Home",
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: SvgPicture.asset("assets/icons/search.svg"),
              ),
              activeIcon: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: SvgPicture.asset(
                  "assets/icons/search.svg",
                  color: ColorPalette.darkColor,
                ),
              ),
              label: "Shop",
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: SvgPicture.asset("assets/icons/heart.svg"),
              ),
              activeIcon: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: SvgPicture.asset(
                  "assets/icons/heart.svg",
                  color: ColorPalette.darkColor,
                ),
              ),
              label: "Favorites",
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: SvgPicture.asset("assets/icons/bag.svg"),
              ),
              activeIcon: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: SvgPicture.asset(
                  "assets/icons/bag.svg",
                  color: ColorPalette.darkColor,
                ),
              ),
              label: "Bag",
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: SvgPicture.asset("assets/icons/profile.svg"),
              ),
              activeIcon: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: SvgPicture.asset(
                  "assets/icons/profile.svg",
                  color: ColorPalette.darkColor,
                ),
              ),
              label: "Profile",
            ),
          ],
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}
