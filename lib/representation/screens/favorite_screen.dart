import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nike_app_clone/controllers/bag_controller.dart';
import 'package:nike_app_clone/controllers/favorites_controller.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';
import 'package:nike_app_clone/representation/widgets/button_widget.dart';
import 'package:nike_app_clone/representation/widgets/single_cart_product_widget.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  static String routeName = "/favorite_screen";

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  final FavoritesController favoritesController =
      Get.put(FavoritesController());
  final BagController bagController = Get.put(BagController());

  @override
  void initState() {
    super.initState();
    favoritesController.getFavoriteProducts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      bottomNavigationBar: Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
        padding: EdgeInsets.only(bottom: 20),
        height: 80,
        width: double.maxFinite,
        child: ButtonWidget(
          onTap: (() => {
                bagController.addToBag(),
              }),
          isFill: true,
          title: "Add to Bag",
          isDark: true,
        ),
      ),
      body: Obx(() {
        if (favoritesController.favoriteProducts.isEmpty) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 24),
                  child: Container(
                    alignment: Alignment.topLeft,
                    child: Text('Favorites',
                        style: TextStyles.defaultStyle.medium
                            .setColor(ColorPalette.darkColor)
                            .setTextSize(28)),
                  ),
                ),
                Container(
                    height: 500,
                    child: Center(
                      child: Text(
                        'No products added to the favorites yet.',
                        style: TextStyles.defaultStyle.regular
                            .setColor(ColorPalette.darkColor)
                            .setTextSize(16),
                      ),
                    ),
                  ),
                SizedBox(
                  height: 50,
                )
              ],
            ),
          );
        }

        return SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 24),
                  child: Container(
                    alignment: Alignment.topLeft,
                    child: Text('Favorites',
                        style: TextStyles.defaultStyle.medium
                            .setColor(ColorPalette.darkColor)
                            .setTextSize(28)),
                  ),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: favoritesController.favoriteProducts.length,
                  itemBuilder: (BuildContext context, int index) {
                    return SingleCartProduct(
                      product: favoritesController.favoriteProducts[index],
                    );
                  },
                ),
                SizedBox(
                  height: 50,
                )
              ],
            ),
          ),
        );
      }),
    );
  }
}
