import 'package:flutter/material.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/representation/screens/intro_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  static String routeName = "/splash_screen";

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    redirectIntroScreen();
  }

  void redirectIntroScreen() async {
    await Future.delayed((const Duration(seconds: 1)));
    Navigator.of(context).pushNamed(IntroScreen.routeName);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: ColorPalette.darkColor,
      ),
      child: Center(child: 
          Image.asset('assets/images/nike-lg-logo.png')
      ),
    );
  }
}