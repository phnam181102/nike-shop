import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:nike_app_clone/controllers/profile_controller.dart';

import '../../core/constants/color_constants.dart';
import '../../core/constants/textstyle_constants.dart';

class EditProfileScreen extends StatefulWidget {
  const EditProfileScreen({Key? key}) : super(key: key);

  static String routeName = "/edit_profile";

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  final ProfileController profileController = Get.put(ProfileController());

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController firstNameController = TextEditingController();
  final TextEditingController lastNameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController addressController = TextEditingController();

  @override
  void initState() {
    profileController.getUserData();
    super.initState();
    firstNameController.text = profileController.user['firstName'] ?? '';
    lastNameController.text = profileController.user['lastName'] ?? '';
    emailController.text = profileController.user['email'] ?? '';
    phoneNumberController.text = profileController.user['phoneNumber'] ?? '';
    addressController.text = profileController.user['address'] ?? '';
  }

  void validation() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      String firstName = firstNameController.text.trim();
      String lastName = lastNameController.text.trim();
      String email = emailController.text.trim();
      String phoneNumber = phoneNumberController.text.trim();
      String address = addressController.text.trim();

      profileController.updateProfile(
        firstName: firstName,
        lastName: lastName,
        email: email,
        phoneNumber: phoneNumber,
        address: address,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Color.fromARGB(255, 255, 255, 255),
        leading: IconButton(
          icon: SvgPicture.asset("assets/icons/left-arrow.svg"),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        title: Text("Edit Profile",
            style: TextStyles.defaultStyle.medium
                .setColor(ColorPalette.darkColor)
                .setTextSize(18)),
      ),
      body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 120,
                      child: Stack(
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 28.0),
                            child: Obx(
                              () => CircleAvatar(
                                backgroundImage: (profileController
                                            .pickedImage !=
                                        null)
                                    ? FileImage(profileController.pickedImage!)
                                    : NetworkImage(
                                            '${profileController.user['profilePhoto']}')
                                        as ImageProvider<Object>?,
                                backgroundColor: Colors.grey[350],
                                radius: 56,
                              ),
                            ),
                          ),
                          Positioned(
                              bottom: 0,
                              right: 25,
                              child: Container(
                                height: 32,
                                width: 32,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                    width: 2,
                                    color: Theme.of(context)
                                        .scaffoldBackgroundColor,
                                  ),
                                  color: Color.fromARGB(255, 149, 149, 149),
                                ),
                                child: GestureDetector(
                                  onTap: () => profileController.pickImage(),
                                  child: Icon(
                                    Icons.add_a_photo,
                                    color: ColorPalette.whiteColor,
                                    size: 15,
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 35,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 165,
                      child: TextFormField(
                        decoration: InputDecoration(
                          hintText: "First name",
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 18),
                          filled: true,
                          fillColor: Colors.black.withOpacity(0.05),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(14),
                            borderSide: BorderSide.none,
                          ),
                        ),
                        controller: firstNameController,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return ("Please enter your name.");
                          }
                        },
                      ),
                    ),
                    Container(
                      width: 165,
                      child: TextFormField(
                        decoration: InputDecoration(
                          hintText: "Last name",
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 18),
                          filled: true,
                          fillColor: Colors.black.withOpacity(0.05),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(14),
                            borderSide: BorderSide.none,
                          ),
                        ),
                        controller: lastNameController,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return ("Please enter your name.");
                          }
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: InputDecoration(
                      hintText: "Email",
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 18),
                      filled: true,
                      fillColor: Colors.black.withOpacity(0.05),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(14),
                        borderSide: BorderSide.none,
                      )),
                  controller: emailController,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return ("Please enter a valid email address.");
                    }
                    if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-z0-9A-Z.-]+.[a-z]")
                        .hasMatch(value)) {
                      return ("Please enter a valid email address.");
                    }
                    return null;
                  },
                  onSaved: (value) {
                    emailController.text = value!;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: InputDecoration(
                      hintText: "Phone number",
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 18),
                      filled: true,
                      fillColor: Colors.black.withOpacity(0.05),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(14),
                        borderSide: BorderSide.none,
                      )),
                  controller: phoneNumberController,
                  onSaved: (value) {
                    phoneNumberController.text = value!;
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                TextFormField(
                  decoration: InputDecoration(
                      hintText: "Address",
                      contentPadding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 18),
                      filled: true,
                      fillColor: Colors.black.withOpacity(0.05),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(14),
                        borderSide: BorderSide.none,
                      )),
                  controller: addressController,
                  onSaved: (value) {
                    addressController.text = value!;
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(top: 55),
                  child: Column(
                    children: [
                      ElevatedButton(
                        onPressed: () => validation(),
                        style: ElevatedButton.styleFrom(
                          primary: ColorPalette.darkColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(100.0),
                          ),
                        ),
                        child: SizedBox(
                          width: 335,
                          height: 56,
                          child: Center(
                            child: Text('Update',
                                style: TextStyles.defaultStyle.semibold
                                    .setColor(ColorPalette.whiteColor)
                                    .setTextSize(17)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ]),
    );
  }
}
