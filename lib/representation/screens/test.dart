import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nike_app_clone/controllers/search_controller.dart';
import 'package:nike_app_clone/models/product.dart';

class SearchPage extends StatelessWidget {
  final SearchController searchController = Get.put(SearchController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Search'),
      ),
      body: Column(
        children: [
          SizedBox(height: 16),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: TextField(
              autofocus: true,
              decoration: InputDecoration(
                hintText: 'Search products',
                border: OutlineInputBorder(),
              ),
              onChanged: (value) {
                searchController.searchProduct(value);
              },
            ),
          ),
          SizedBox(height: 16),
          Expanded(
            child: Obx(() {
              if (searchController.searchedProducts.isEmpty) {
                return Center(
                  child: Text('No results found.'),
                );
              } else {
                return ListView.builder(
                  itemCount: searchController.searchedProducts.length,
                  itemBuilder: (context, index) {
                    Product product = searchController.searchedProducts[index];
                    return ListTile(
                      leading: Image.network(product.thumbnail),
                      title: Text(product.name),
                      subtitle: Text(product.category),
                      // subtitle: Text(product.description),
                      trailing: Text(product.price),
                    );
                  },
                );
              }
            }),
          ),
        ],
      ),
    );
  }
}
