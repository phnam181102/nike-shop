import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:nike_app_clone/constants.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';
import 'package:nike_app_clone/representation/screens/intro_screen.dart';
import 'package:nike_app_clone/representation/screens/main_app.dart';
import 'package:nike_app_clone/representation/screens/signin_screen.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  static String routeName = "/signup_screen";

  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final TextEditingController firstNameController = TextEditingController();

  final TextEditingController lastNameController = TextEditingController();

  final TextEditingController emailController = TextEditingController();

  final TextEditingController passwordController = TextEditingController();

  final TextEditingController reenterPasswordController =
      TextEditingController();

  bool isChecked = false;

  bool _passwordVisible = false;

  bool _passwordVisible2 = false;

  String email = '';
  String password = '';
  String reenterPassword = '';

  void validation() async {
    if (_formKey.currentState!.validate()) {
      authController.registerUser(
          firstNameController.text,
          lastNameController.text,
          emailController.text,
          passwordController.text);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.fromLTRB(0, 92, 0, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 20),
                child: Image.asset('assets/images/nike-sm-logo-black.png',
                    width: 53, height: 19),
              ),
              Padding(
                  padding: EdgeInsets.only(left: 20, top: 28),
                  child: SizedBox(
                    child: Text(
                      "Now let's make you a\nNike Member.",
                      style: TextStyles.defaultStyle.medium
                          .setColor(ColorPalette.darkColor)
                          .setTextSize(28),
                    ),
                  )),
              Padding(
                padding: EdgeInsets.only(left: 20, top: 46, right: 20),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: 165,
                            child: TextFormField(
                              decoration: InputDecoration(
                                hintText: "First name",
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 18),
                                filled: true,
                                fillColor: Colors.black.withOpacity(0.05),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(14),
                                  borderSide: BorderSide.none,
                                ),
                              ),
                              controller: firstNameController,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return ("Please enter your name.");
                                }
                              },
                            ),
                          ),
                          Container(
                            width: 165,
                            child: TextFormField(
                              decoration: InputDecoration(
                                hintText: "Last name",
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20, vertical: 18),
                                filled: true,
                                fillColor: Colors.black.withOpacity(0.05),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(14),
                                  borderSide: BorderSide.none,
                                ),
                              ),
                              controller: lastNameController,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return ("Please enter your name.");
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            hintText: "Email",
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 18),
                            filled: true,
                            fillColor: Colors.black.withOpacity(0.05),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(14),
                              borderSide: BorderSide.none,
                            )),
                        onChanged: (value) => {
                          setState(() {
                            email = value;
                          })
                        },
                        controller: emailController,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return ("Please enter a valid email address.");
                          }
                          if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-z0-9A-Z.-]+.[a-z]")
                              .hasMatch(value)) {
                            return ("Please enter a valid email address.");
                          }
                          return null;
                        },
                        onSaved: (value) {
                          emailController.text = value!;
                        },
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                            hintText: "Password",
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 18),
                            suffixIcon: IconButton(
                              icon: Icon(
                                // Based on passwordVisible state choose the icon
                                _passwordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: Theme.of(context).primaryColorDark,
                              ),
                              onPressed: () {
                                // Update the state i.e. toogle the state of passwordVisible variable
                                setState(() {
                                  _passwordVisible = !_passwordVisible;
                                });
                              },
                            ),
                            filled: true,
                            fillColor: Colors.black.withOpacity(0.05),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(14),
                              borderSide: BorderSide.none,
                            )),
                        onChanged: (value) => {
                          setState(() {
                            password = value;
                          })
                        },
                        controller: passwordController,
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          RegExp regex = RegExp(r'^.{8,}$');
                          if (value!.isEmpty) {
                            return ("Password does not meet minimum requirements.");
                          }
                          if (!regex.hasMatch(value)) {
                            return ("Minimum of 8 characters");
                          }
                        },
                        onSaved: (value) {
                          passwordController.text = value!;
                        },
                        obscureText: !_passwordVisible,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          hintText: "Re-enter Password",
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 18),
                          suffixIcon: IconButton(
                            icon: Icon(
                              // Based on passwordVisible2 state choose the icon
                              _passwordVisible2
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Theme.of(context).primaryColorDark,
                            ),
                            onPressed: () {
                              // Update the state i.e. toggle the state of passwordVisible2 variable
                              setState(() {
                                _passwordVisible2 = !_passwordVisible2;
                              });
                            },
                          ),
                          filled: true,
                          fillColor: Colors.black.withOpacity(0.05),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(14),
                            borderSide: BorderSide.none,
                          ),
                        ),
                        onChanged: (value) {
                          setState(() {
                            reenterPassword = value;
                          });
                        },
                        controller: reenterPasswordController,
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Please re-enter your password.";
                          }
                          if (value != password) {
                            return "Passwords do not match.";
                          }
                          return null;
                        },
                        onSaved: (value) {
                          reenterPasswordController.text = value!;
                        },
                        obscureText: !_passwordVisible2,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Row(children: [
                        Checkbox(
                          shape: const CircleBorder(),
                          checkColor: ColorPalette.whiteColor,
                          activeColor: ColorPalette.darkColor,
                          value: isChecked,
                          onChanged: (bool? newValue) {
                            setState(() {
                              isChecked = newValue!;
                            });
                          },
                        ),
                        RichText(
                          text: new TextSpan(
                            text: "I agree to the ",
                            style: TextStyles.defaultStyle.medium
                                .setColor(ColorPalette.subColor)
                                .setTextSize(13),
                            children: [
                              TextSpan(
                                text: 'Privacy policy',
                                style: TextStyle(
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                              TextSpan(
                                text: ' and ',
                              ),
                              TextSpan(
                                text: 'Terms of Use',
                                style: TextStyle(
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ]),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 20, top: 32, right: 20),
                child: Column(
                  children: [
                    Material(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100.0)),
                      color: Color.fromARGB(170, 0, 0, 0),
                      clipBehavior: Clip.antiAlias,
                      child: MaterialButton(
                        onPressed: isChecked ? (validation) : null,
                        color: ColorPalette.darkColor,
                        textColor: ColorPalette.whiteColor,
                        child: SizedBox(
                          width: 335,
                          height: 56,
                          child: Center(
                            child: Text('Join Us',
                                style: TextStyles.defaultStyle.semibold
                                    .setColor(ColorPalette.whiteColor)
                                    .setTextSize(17)),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Already have an account?",
                          style: TextStyles.defaultStyle.medium
                              .setColor(ColorPalette.subColor)
                              .setTextSize(15),
                        ),
                        SizedBox(
                          width: 4,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .pushNamed(SignInScreen.routeName);
                          },
                          child: Text("Sign In",
                              style: TextStyles.defaultStyle.medium
                                  .setColor(ColorPalette.darkColor)
                                  .setTextSize(15)),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
