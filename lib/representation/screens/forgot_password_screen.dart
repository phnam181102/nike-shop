import 'package:flutter/material.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';
import 'package:nike_app_clone/representation/screens/intro_screen.dart';
import 'package:nike_app_clone/representation/screens/signin_screen.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  static String routeName = "/forgot_password_screen";

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Center(
            child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset('assets/images/nike-sm-logo-black.png', width: 53, height: 19),
              SizedBox(
                height: 28,
              ),
              Text(
                "RESET PASSWORD",
                style: TextStyles.defaultStyle.semibold.setColor(ColorPalette.darkColor).setTextSize(28),
              ),
              SizedBox(
                height: 32,
              ),
              Text("Enter your email to receive instructions on how to reset your password.", style: TextStyles.defaultStyle.setColor(ColorPalette.subColor).setTextSize(17), textAlign: TextAlign.center,),
              SizedBox(
                height: 32,
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Text(
                      '',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.black12,
                      ),
                    ),
                    TextFormField(
                      decoration: InputDecoration(
                          hintText: "Email address",
                          contentPadding: EdgeInsets.all(20),
                          filled: true,
                          fillColor: Colors.black.withOpacity(0.05),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(14),
                            borderSide: BorderSide.none,
                          )),
                      controller: emailController,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return ("Please enter a valid email address.");
                        }
                        if (!RegExp(
                                "^[a-zA-Z0-9+_.-]+@[a-z0-9A-Z.-]+.[a-z]")
                            .hasMatch(value)) {
                          return ("Please enter a valid email address.");
                        }
                        return null;
                      },
                      onSaved: (value) {
                        emailController.text = value!;
                      },
                    ),
                    Text(
                      '',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 32,
              ),
              ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    Navigator.of(context)
                        .pushNamed(IntroScreen.routeName);
                  }
                },
                style: ElevatedButton.styleFrom(
                    primary: ColorPalette.darkColor,
                    shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(100.0),
                ),),
                child: SizedBox(
                  width: 335,
                  height: 56,
                  child: Center(
                    child: Text(
                      'Reset',
                      style: TextStyles.defaultStyle.semibold.setColor(ColorPalette.whiteColor).setTextSize(17)
                    ),
                  ),
                ),
              ),
              SizedBox(
                          height: 40,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Or return to",
                                  style: TextStyles.defaultStyle.medium.setColor(ColorPalette.subColor).setTextSize(15),),
                            SizedBox(
                              width: 4,
                            ),
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context)
                                    .pushNamed(SignInScreen.routeName);
                              },
                              child: Text(
                                "Log In",
                                style: TextStyles.defaultStyle.medium.setColor(ColorPalette.darkColor).setTextSize(15)
                              ),
                            )
                          ],
                        ),
                      
            ],
          ),
          )
        ),
    
    );
  }
}