import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nike_app_clone/controllers/bag_controller.dart';
import 'package:nike_app_clone/controllers/favorites_controller.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';
import 'package:nike_app_clone/representation/widgets/button_widget.dart';
import 'package:nike_app_clone/representation/widgets/single_cart_product_widget.dart';

import '../../models/bag_item.dart';
import '../../models/product.dart';

class BagScreen extends StatefulWidget {
  @override
  _BagScreenState createState() => _BagScreenState();
}

class _BagScreenState extends State<BagScreen> {
  final FavoritesController favoritesController =
      Get.put(FavoritesController());
  final BagController bagController = Get.put(BagController());

  @override
  void initState() {
    bagController.getBagProducts();
    bagController.updateTotalValue();
    super.initState();
  }

  @override
  void dispose() {
    bagController.clearSelectedProducts();
    super.dispose();
  }

  double get subtotal => bagController.totalValue.toDouble();
  double get salesTax => subtotal * 0.08;
  double get fees => subtotal * 0.02;
  double get total => subtotal + salesTax + fees;

  String formatNumber(double number) {
    if (number % 1 == 0) {
      return number.toInt().toString();
    } else {
      return number.toStringAsFixed(2);
    }
  }

  String get formattedSubtotal => formatNumber(subtotal);
  String get formattedSalesTax => formatNumber(salesTax);
  String get formattedFees => formatNumber(fees);
  String get formattedTotal => formatNumber(total);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: true,
      bottomNavigationBar: Obx(() {
        if (bagController.selectedProducts.isEmpty) {
          return Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            padding: EdgeInsets.only(bottom: 20),
            height: 80,
            width: double.maxFinite,
            child: ButtonWidget(
              onTap: (() => bagController.addToBag()),
              isFill: true,
              title: "Checkout",
              isDark: true,
            ),
          );
        }

        return Container(
          decoration: BoxDecoration(
          color: ColorPalette.whiteColor,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12),
            topRight: Radius.circular(12),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 2,
              blurRadius: 5,
              offset: Offset(0, -3),
            ),
          ],
        ),
          height: 255,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(20, 12, 20, 20),
                child: Column(children: [
                  buildFeeRow("Subtotal", subtotal),
                  buildFeeRow("Sales Tax (8%)", salesTax),
                  buildFeeRow("Fees", fees),
                  Divider(),
                  buildFeeRow("Total", total, isTotal: true),
                ]),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                padding: EdgeInsets.only(bottom: 20),
                height: 80,
                width: double.maxFinite,
                child: ButtonWidget(
                  onTap: () {},
                  isFill: true,
                  title: "Checkout",
                  isDark: true,
                ),
              ),
            ],
          ),
        );
      }),
      body: Obx(() {
        if (bagController.productList.isEmpty) {
          return Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 24),
                  child: Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Bag',
                      style: TextStyles.defaultStyle.medium
                          .setColor(ColorPalette.darkColor)
                          .setTextSize(28),
                    ),
                  ),
                ),
                Container(
                  height: 500,
                  child: Center(
                    child: Text(
                      'No products added to the bag yet.',
                      style: TextStyles.defaultStyle.regular
                          .setColor(ColorPalette.darkColor)
                          .setTextSize(16),
                    ),
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          );
        }

        return SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 12),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 10, 0, 24),
                  child: Container(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Bag',
                      style: TextStyles.defaultStyle.medium
                          .setColor(ColorPalette.darkColor)
                          .setTextSize(28),
                    ),
                  ),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: bagController.productList.length,
                  itemBuilder: (BuildContext context, int index) {
                    final Product product = bagController.productList[index];
                    return SingleCartProduct(
                      product: product,
                      quantity: getQuantity(product.id),
                    );
                  },
                ),
                SizedBox(
                  height: 50,
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

  Widget buildFeeRow(String title, double value, {bool isTotal = false}) {
    return Padding(
      padding: EdgeInsets.only(top: 5,bottom: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          isTotal
              ? Text(title,
                  style: TextStyles.defaultStyle.bold
                      .setColor(ColorPalette.darkColor)
                      .setTextSize(20))
              : Text(title,
                  style: TextStyles.defaultStyle.medium
                      .setColor(ColorPalette.darkColor)
                      .setTextSize(17)),
          isTotal
              ? Text('\$${formatNumber(value)}',
                  style: TextStyles.defaultStyle.semibold
                      .setColor(ColorPalette.darkColor)
                      .setTextSize(18))
              : Text('\$${formatNumber(value)}',
                  style: TextStyles.defaultStyle.regular
                      .setColor(ColorPalette.darkColor)
                      .setTextSize(16)),
        ],
      ),
    );
  }

  int getQuantity(String productId) {
    final List<BagItem> bagProducts = bagController.bagProducts;
    int quantity = 0;

    for (final item in bagProducts) {
      if (item.productId == productId) {
        quantity += item.quantity;
      }
    }

    return quantity;
  }
}
