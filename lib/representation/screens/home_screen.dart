import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/core/constants/textstyle_constants.dart';
import 'package:nike_app_clone/home-list-items.dart';

var date = DateTime.now();
String formattedDate = DateFormat('EEEE, MMMM d').format(date);

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  static String routeName = "/home_screen";

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorPalette.whiteColor,
      body: 
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(24, 10, 0, 24),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Discover", 
                    style: TextStyles.defaultStyle.medium.setColor(ColorPalette.darkColor).setTextSize(28)),
                    SizedBox(
                      height: 11,
                    ),
                    Text(formattedDate,
                    style: TextStyles.defaultStyle.medium.setColor(ColorPalette.subColor).setTextSize(16.5))
                  ],
                )
              )
              ,Expanded(
                child: ListView.builder(
                  itemCount: items.length,
                  itemBuilder: (context, index) {
                    final  item = items[index];
                    return HomeListItems(item: item);
                  }
                ),
              ),
            ],
          )
    );
  }
}

class HomeListItems extends StatelessWidget {
  final ListItems item;
  const HomeListItems({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 3),
      child: SizedBox(
        height: 480,
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Positioned.fill(
              child: ClipRRect(
                child: Image.network(
                  item.image,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Positioned(
              child: Container(
                padding: EdgeInsets.fromLTRB(22, 30, 22, 32), 
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(padding: EdgeInsets.only(bottom: 8),
                      child: Text(item.subTitle, style: item.color == "dark" ? TextStyles.defaultStyle.darkColor.setTextSize(14) : TextStyles.defaultStyle.whiteColor.setTextSize(14),),
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 16),
                      child: Text(item.title, style: item.color == "dark" ? TextStyles.defaultStyle.black.darkColor.setTextSize(32) : TextStyles.defaultStyle.black.whiteColor.setTextSize(32)),
                    ),
                    Row(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.symmetric(vertical: 12, horizontal: 18),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: item.color == "dark" ? ColorPalette.darkColor : ColorPalette.whiteColor,
                          ),
                          child: Text(
                            item.buttonTitle,
                            style: item.color == "dark" ? TextStyles.defaultStyle.bold.whiteColor.setTextSize(14) : TextStyles.defaultStyle.bold.darkColor.setTextSize(14),
                          ),
                        ),
                      ],
                    )
                  ],
                ))
              )
          ],
        ),
      ),
    );
  }
}