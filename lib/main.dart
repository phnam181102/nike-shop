import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:nike_app_clone/core/constants/color_constants.dart';
import 'package:nike_app_clone/representation/screens/main_app.dart';
import 'package:nike_app_clone/representation/screens/add_product_screen.dart';
import 'package:nike_app_clone/representation/screens/splash_screen.dart';
import 'package:nike_app_clone/routes.dart';
import 'controllers/auth_controller.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp().then((value) {
    Get.put(AuthController());
  });
  final fcmToken = await FirebaseMessaging.instance.getToken();
  print(fcmToken);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        title: 'Nike',
        theme: ThemeData(
          fontFamily: 'Inter',
          primaryColor: ColorPalette.darkColor,
          scaffoldBackgroundColor: ColorPalette.whiteColor,
          backgroundColor: ColorPalette.whiteColor,
        ),
        debugShowCheckedModeBanner: false,
        routes: routes,
        home: const SplashScreen());
  }
}
