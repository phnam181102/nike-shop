import 'package:cloud_firestore/cloud_firestore.dart';

class Comment {
  int rating;
  String content;
  DateTime timestamp;
  String userId;

  Comment({
    required this.rating,
    required this.content,
    required this.timestamp,
    required this.userId,
  });

  static Comment fromJson(Map<String, dynamic> json) {
    return Comment(
      rating: json['rating'],
      content: json['content'],
      timestamp: (json['timestamp'] as Timestamp).toDate(),
      userId: json['userId'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'rating': rating,
      'content': content,
      'timestamp': Timestamp.fromDate(timestamp),
      'userId': userId,
    };
  }
}
