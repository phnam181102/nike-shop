import 'package:cloud_firestore/cloud_firestore.dart';

class BagItem {
  int quantity;
  String productId;
  int price; 

  BagItem({
    required this.quantity,
    required this.productId,
    required this.price, 
  });

  Map<String, dynamic> toJson() => {
        "quantity": quantity,
        "productId": productId,
        "price": price, 
      };

  static BagItem fromJson(Map<String, dynamic> json) {
    return BagItem(
      quantity: json['quantity'],
      productId: json['productId'],
      price: json['price'], 
    );
  }

  static BagItem fromSnap(DocumentSnapshot snap) {
    var snapshot = snap.data() as Map<String, dynamic>;
    return BagItem(
      quantity: snapshot['quantity'],
      productId: snapshot['productId'],
      price: snapshot['price'], 
    );
  }
}
