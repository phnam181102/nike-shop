import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  String firstName;
  String lastName;
  String profilePhoto;
  String email;
  String phoneNumber; 
  String address; 
  String uid;
  DateTime createdDate;

  User({
    required this.firstName,
    required this.lastName,
    required this.profilePhoto,
    required this.email,
    required this.phoneNumber,
    required this.address,
    required this.uid,
    required this.createdDate,
  });

  Map<String, dynamic> toJson() => {
        "firstName": firstName,
        "lastName": lastName,
        "profilePhoto": profilePhoto,
        "email": email,
        "phoneNumber": phoneNumber,
        "address": address,
        "uid": uid,
        "createdDate": createdDate,
      };

  static User fromSnap(DocumentSnapshot snap) {
    var snapshot = snap.data() as Map<String, dynamic>;
    return User(
      email: snapshot['email'],
      profilePhoto: snapshot['profilePhoto'],
      uid: snapshot['uid'],
      firstName: snapshot['firstName'],
      lastName: snapshot['lastName'],
      phoneNumber: snapshot['phoneNumber'], 
      address: snapshot['address'],
      createdDate: snapshot['createdDate'].toDate(),
    );
  }
}
