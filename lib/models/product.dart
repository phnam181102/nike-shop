import 'package:cloud_firestore/cloud_firestore.dart';

class Product {
  String name;
  String category;
  String description;
  String thumbnail;
  List<String> detailImgs;
  String price;
  String id;

  Product({
    required this.name,
    required this.category,
    required this.description,
    required this.thumbnail,
    required this.detailImgs,
    required this.price,
    required this.id,
  });

  Map<String, dynamic> toJson() => {
        "name": name,
        "category": category,
        "description": description,
        "thumbnail": thumbnail,
        "detailImgs": detailImgs,
        "price": price,
        "id": id,
      };

  static Product fromJson(Map<String, dynamic> json) {
    return Product(
      name: json['name'],
      category: json['category'],
      description: json['description'],
      detailImgs: List<String>.from(json['detailImgs']),
      thumbnail: json['thumbnail'],
      price: json['price'],
      id: json['id'],
    );
  }

  static Product fromSnap(DocumentSnapshot snap) {
    var snapshot = snap.data() as Map<String, dynamic>;
    return Product(
      name: snapshot['name'],
      category: snapshot['category'],
      description: snapshot['description'],
      detailImgs: List<String>.from(snapshot['detailImgs']),
      thumbnail: snapshot['thumbnail'],
      price: snapshot['price'],
      id: snapshot['id'],
    );
  }
}
