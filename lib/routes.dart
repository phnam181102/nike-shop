import 'package:flutter/material.dart';
import 'package:nike_app_clone/representation/screens/forgot_password_screen.dart';
import 'package:nike_app_clone/representation/screens/intro_screen.dart';
import 'package:nike_app_clone/representation/screens/list_product_screen.dart';
import 'package:nike_app_clone/representation/screens/main_app.dart';
import 'package:nike_app_clone/representation/screens/signin_screen.dart';
import 'package:nike_app_clone/representation/screens/signup_screen.dart';
import 'package:nike_app_clone/representation/screens/splash_screen.dart';


final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => const SplashScreen(),
  IntroScreen.routeName: (context) => const IntroScreen(),
  SignUpScreen.routeName: (context) => const SignUpScreen(),
  SignInScreen.routeName: (context) => const SignInScreen(),
  ForgotPasswordScreen.routeName: (context) => const ForgotPasswordScreen(),
  MainApp.routeName: (context) => const MainApp(),
};


