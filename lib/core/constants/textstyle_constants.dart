import 'package:flutter/material.dart';
import 'package:nike_app_clone/core/constants/color_constants.dart';

extension ExtendedTextStyle on TextStyle {
  TextStyle get light {
    return copyWith(fontWeight: FontWeight.w300);
  }

  TextStyle get regular {
    return copyWith(fontWeight: FontWeight.w400);
  }

  TextStyle get italic {
    return copyWith(
      fontWeight: FontWeight.normal,
      fontStyle: FontStyle.italic,
    );
  }

  TextStyle get medium {
    return copyWith(fontWeight: FontWeight.w500);
  }

  TextStyle get semibold {
    return copyWith(fontWeight: FontWeight.w600);
  }

  TextStyle get bold {
    return copyWith(fontWeight: FontWeight.w700);
  }

  TextStyle get extrabold {
    return copyWith(fontWeight: FontWeight.w800);
  }

  TextStyle get black {
    return copyWith(fontWeight: FontWeight.w900);
  }

  TextStyle get fontHeader {
    return copyWith(
      fontSize: 22,
      height: 22 / 20,
    );
  }

  TextStyle get fontCaption {
    return copyWith(
      fontSize: 13.5,
      height: 12 / 8,
    );
  }

  TextStyle get darkColor {
    return copyWith(color: ColorPalette.darkColor);
  }

  TextStyle get subColor {
    return copyWith(color: ColorPalette.subColor);
  }

  TextStyle get whiteColor {
    return copyWith(color: Colors.white);
  }

  // convenience functions
  TextStyle setColor(Color color) {
    return copyWith(color: color);
  }

  TextStyle setTextSize(double size) {
    return copyWith(fontSize: size);
  }

  TextStyle setTextHeight(double height) {
    return copyWith(height: height);
  }
}

class TextStyles {
  TextStyles(this.context);

  BuildContext? context;

  static const TextStyle defaultStyle = TextStyle(
    fontFamily: 'Inter',
    decoration: TextDecoration.none,
    fontSize: 15,
    color: ColorPalette.subColor,
    fontWeight: FontWeight.w400,
    height: 16 / 15,
  );
}

// How to use?
// Text('test text', style: TextStyles.normalText.semibold.whiteColor);
// Text('test text', style: TextStyles.itemText.whiteColor.bold);