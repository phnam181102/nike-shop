import 'package:flutter/material.dart';

class ColorPalette {
  static const Color darkColor = Color(0xff000000);
  static const Color subColor = Color(0xff747474);
  static const Color whiteColor = Color(0xffFFFFFF);
}
