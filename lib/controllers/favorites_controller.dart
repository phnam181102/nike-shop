import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:nike_app_clone/constants.dart';
import 'package:nike_app_clone/models/product.dart';

class FavoritesController extends GetxController {
  final Rx<List<Product>> _productList = Rx<List<Product>>([]);
  final RxList<Product> _favoriteProducts = RxList<Product>([]);

  List<Product> get productList => _productList.value;
  List<Product> get favoriteProducts => _favoriteProducts;

  @override
  void onReady() {
    getFavoriteProducts();
    _productList.bindStream(
        firestore.collection('products').snapshots().map((QuerySnapshot query) {
      List<Product> result = [];
      for (var element in query.docs) {
        result.add(
          Product.fromSnap(element),
        );
      }
      return result;
    }));
    super.onReady();
  }

  addToFavorites(String productId) async {
    try {
      await firestore.collection('favorites').doc(authController.user.uid).set({
        'user': authController.user.uid,
        'products': FieldValue.arrayUnion([productId])
      }, SetOptions(merge: true));
      getFavoriteProducts();
      Get.snackbar(
        'Success',
        'Amazing! Product has been Added To Favorite Successfully.',
        icon: SvgPicture.asset("assets/icons/success.svg"),
        margin: EdgeInsets.all(15),
        snackPosition: SnackPosition.TOP,
      );
    } catch (e) {
      print('Error adding product to favorites: $e');
    }
  }

  removeFromFavorites(String productId) async {
    try {
      await firestore.collection('favorites').doc(authController.user.uid).update({
        'products': FieldValue.arrayRemove([productId])
      });
      getFavoriteProducts();
    } catch (e) {
      print('Error removing product from favorites: $e');
    }
  }

  void getFavoriteProducts() async {
    final favorites =
        await firestore.collection('favorites').doc(authController.user.uid).get();
    if (!favorites.exists ||
        favorites.data() == null ||
        favorites.data()!['products'] == null) {
      _favoriteProducts.clear();
      return;
    }

    try {
      final productIds = List<String>.from(favorites.data()!['products'] as List);
      List<Product> products = [];
      await Future.forEach(productIds, (String productId) async {
        final docSnapshot =
            await firestore.collection('products').doc(productId).get();
        if (docSnapshot.exists) {
          Product product = Product.fromSnap(docSnapshot);
          products.add(product);
        }
      });
      _favoriteProducts.assignAll(products);
    } catch (e) {
      print('Error - $e');
    }
  }
}
