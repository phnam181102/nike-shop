import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:nike_app_clone/constants.dart';
import 'package:nike_app_clone/models/bag_item.dart';
import 'package:nike_app_clone/models/product.dart';

class BagController extends GetxController {
  final RxList<BagItem> _selectedProducts = RxList<BagItem>([]);
  final RxList<BagItem> _bagProducts = RxList<BagItem>([]);
  final RxList<Product> _productList = RxList<Product>([]);

  RxInt totalValue = 0.obs;

  List<BagItem> get selectedProducts => _selectedProducts;
  List<BagItem> get bagProducts => _bagProducts;
  List<Product> get productList => _productList;

  @override
  void onReady() {
    getBagProducts();
    super.onReady();
  }

  void clearSelectedProducts() {
    _selectedProducts.clear();
  }

  void addToSelectedProducts(String productId, int quantity, int price) {
    _selectedProducts
        .add(BagItem(quantity: quantity, productId: productId, price: price));
    updateTotalValue();
  }

  void removeFromSelectedProducts(String productId) {
    _selectedProducts.removeWhere((item) => item.productId == productId);
    updateTotalValue();
  }

  void updateQuantityInBag(String productId, int newQuantity) async {
    try {
      final bagDoc = FirebaseFirestore.instance
          .collection('bag')
          .doc(authController.user.uid);
      final bagSnapshot = await bagDoc.get();

      if (bagSnapshot.exists) {
        final List<dynamic> existingProducts =
            List<dynamic>.from(bagSnapshot.data()!['products']);

        for (int i = 0; i < existingProducts.length; i++) {
          final existingProduct = existingProducts[i];
          if (existingProduct['productId'] == productId) {
            existingProduct['quantity'] = newQuantity;
            break;
          }
        }

        await bagDoc.update({'products': existingProducts});

        // Update bagProducts and productList
        getBagProducts();
      }

      // Update selectedProducts
      final updatedSelectedProducts = _selectedProducts.map((product) {
        if (product.productId == productId) {
          return BagItem(
            productId: productId,
            quantity: newQuantity,
            price: product.price,
          );
        } else {
          return product;
        }
      }).toList();

      _selectedProducts.assignAll(updatedSelectedProducts);
      updateTotalValue();
    } catch (e) {
      print('Error updating quantity in bag: $e');
    }
  }

  void updateTotalValue() {
    int sum = 0;
    for (final item in _selectedProducts) {
      sum += item.quantity * item.price;
    }
    totalValue.value = sum;
  }

  void addToBag() async {
    try {
      final bagDoc = FirebaseFirestore.instance
          .collection('bag')
          .doc(authController.user.uid);
      final bag = await bagDoc.get();
      final List<Map<String, dynamic>> products =
          _selectedProducts.map((item) => item.toJson()).toList();

      if (!bag.exists) {
        await bagDoc.set({'products': products});
      } else {
        final List<dynamic> existingProducts = bag.data()!['products'];
        for (final product in products) {
          final String productId = product['productId'];
          final int quantity = product['quantity'];

          bool found = false;
          for (int i = 0; i < existingProducts.length; i++) {
            final existingProduct = existingProducts[i];
            if (existingProduct['productId'] == productId) {
              existingProduct['quantity'] += quantity;
              found = true;
              break;
            }
          }

          if (!found) {
            existingProducts.add(product);
          }
        }

        await bagDoc.update({'products': existingProducts});
      }

      _selectedProducts.clear();
      getBagProducts();

      Get.snackbar(
        'Success',
        'Amazing! The product has been successfully added to your bag.',
        icon: SvgPicture.asset("assets/icons/success.svg"),
        margin: EdgeInsets.all(15),
        snackPosition: SnackPosition.TOP,
      );
    } catch (e) {
      print('Error adding to bag: $e');
    }
  }

  void removeFromBag(String productId) async {
    try {
      final bagDoc = FirebaseFirestore.instance
          .collection('bag')
          .doc(authController.user.uid);
      final bagSnapshot = await bagDoc.get();

      if (bagSnapshot.exists) {
        final List<dynamic> existingProducts =
            List<dynamic>.from(bagSnapshot.data()!['products']);

        existingProducts
            .removeWhere((product) => product['productId'] == productId);

        await bagDoc.update({'products': existingProducts});

        // Update bagProducts and productList
        getBagProducts();
      }
    } catch (e) {
      print('Error removing product from bag: $e');
    }
  }

  void getBagProducts() async {
    try {
      final bagDoc = FirebaseFirestore.instance
          .collection('bag')
          .doc(authController.user.uid);
      final bagSnapshot = await bagDoc.get();

      if (!bagSnapshot.exists) {
        _bagProducts.clear();
        _productList.clear(); // Reset productList to an empty array
        return;
      }

      final List<dynamic> productIds = [];
      final List<Map<String, dynamic>> bagData =
          List<Map<String, dynamic>>.from(bagSnapshot.data()!['products']);

      for (final bagItem in bagData) {
        final String productId = bagItem['productId'];
        productIds.add(productId);
      }

      final productsSnapshot =
          await FirebaseFirestore.instance.collection('products').get();
      final List<Product> products = [];

      for (final productDoc in productsSnapshot.docs) {
        final product = Product.fromSnap(productDoc);

        if (productIds.contains(product.id)) {
          products.add(product);
        }
      }

      final List<BagItem> bagItems = bagData.map((item) {
        final productId = item['productId'];
        final quantity = item['quantity'] as int;
        final price = item['price'] as int;

        return BagItem(productId: productId, quantity: quantity, price: price);
      }).toList();

      _bagProducts.assignAll(bagItems);
      _productList.assignAll(products);
    } catch (e) {
      print('Error getting bag products: $e');
    }
  }
}
