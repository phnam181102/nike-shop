import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:nike_app_clone/constants.dart';
import 'package:nike_app_clone/models/product.dart';

class SearchController extends GetxController {
  final Rx<List<Product>> _searchedProducts = Rx<List<Product>>([]);
  final Rx<String> _typeProduct = Rx<String>('');

  List<Product> get searchedProducts => _searchedProducts.value;
  String get typeProduct => _typeProduct.value;

  @override
  void onReady() {
    searchProduct('');
    super.onReady();
  }

  searchProduct(String typedProduct) async {
    _typeProduct.value = typedProduct;

    _searchedProducts.bindStream(
        firestore.collection('products').snapshots().map((QuerySnapshot query) {
      List<Product> retVal = [];
      for (var elem in query.docs) {
        var product = Product.fromSnap(elem);
        if (typedProduct.isEmpty ||
            product.category
                .toLowerCase()
                .contains(typedProduct.toLowerCase()) ||
            product.name.toLowerCase().contains(typedProduct.toLowerCase())) {
          retVal.add(product);
        }
      }
      return retVal;
    }));

  }
}
