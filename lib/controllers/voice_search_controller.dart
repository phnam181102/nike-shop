import 'package:get/get.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;


class VoiceSearchController extends GetxController {
  final Rx<String> _searchText = Rx<String>('');
  final stt.SpeechToText _speechToText = stt.SpeechToText();
  bool _isListening = false;

  String get searchText => _searchText.value;

  void startListening() async {
    bool isSpeechAvailable = await _speechToText.initialize();
    if (isSpeechAvailable) {
      bool isAvailable = _speechToText.isAvailable;

      if (isAvailable) {
        _isListening = true;
        _speechToText.listen(
          onResult: (result) {
            final String recognizedWords = result.recognizedWords;
            if (result.finalResult && recognizedWords.isNotEmpty) {
              _searchText.value = recognizedWords;
            }
          },
        );
      } else {
        // Thiết bị không hỗ trợ nhận dạng giọng nói
        print('Speech recognition not available on this device');
      }
    } else {
      // Lỗi khởi tạo nhận dạng giọng nói
      print('Failed to initialize speech recognition');
    }
  }

  void stopListening() {
    if (_isListening) {
      _isListening = false;
      _speechToText.stop();
    }
  }
}
