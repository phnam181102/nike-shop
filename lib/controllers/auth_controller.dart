import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:nike_app_clone/constants.dart';
import 'package:nike_app_clone/controllers/profile_controller.dart';
import 'package:nike_app_clone/models/user.dart' as model;
import 'package:nike_app_clone/representation/screens/main_app.dart';
import 'package:nike_app_clone/representation/screens/signin_screen.dart';

class AuthController extends GetxController {
  static AuthController instance = Get.find();
  late Rx<User?> _user;

  User get user => _user.value!;

  @override
  void onInit() {
    super.onInit();
    _user = Rx<User?>(firebaseAuth.currentUser);
    _user.bindStream(firebaseAuth.authStateChanges());
    _setInitialScreen(_user.value); // Chuyển đến màn hình tương ứng khi bắt đầu
  }

  void _setInitialScreen(User? user) async {
    if (user == null) {
      Get.offAll(() => const SignInScreen());
    } else {
      Get.offAllNamed('/main_app');
    }
  }

  void registerUser(
      String firstName, String lastName, String email, String password) async {
    try {
      UserCredential cred = await firebaseAuth.createUserWithEmailAndPassword(
          email: email, password: password);

      Get.off(() => const SignInScreen());

      DateTime now = DateTime.now();
      model.User user = model.User(
          firstName: firstName,
          lastName: lastName,
          email: email,
          phoneNumber: '',
          address: '',
          uid: cred.user!.uid,
          profilePhoto: "https://i.ibb.co/tY7RGPt/ta-i-xu-ng.png",
          createdDate: now);

      await firestore.collection('users').doc(cred.user!.uid).set(user.toJson());

      Get.snackbar(
        'Success',
        'Amazing! Your Account has been Created Successfully.',
        icon: SvgPicture.asset("assets/icons/success.svg"),
        margin: EdgeInsets.all(15),
        snackPosition: SnackPosition.TOP,
      );

    } catch (e) {
      Get.snackbar(
        'Error Creating Account',
        e.toString(),
        icon: SvgPicture.asset("assets/icons/error.svg"),
        margin: EdgeInsets.all(15),
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  void loginUser(String email, String password) async {
    try {
      await firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      _setInitialScreen(firebaseAuth.currentUser);
    } catch (e) {
      Get.snackbar(
        'Login Error',
        e.toString(),
        icon: SvgPicture.asset("assets/icons/error.svg"),
        margin: EdgeInsets.all(15),
        snackPosition: SnackPosition.TOP,
      );
    }
  }

  void logOut() async {
    await firebaseAuth.signOut();

    ProfileController profileController = Get.find<ProfileController>();
    profileController.resetUserData();

    _setInitialScreen(null);
  }
}
