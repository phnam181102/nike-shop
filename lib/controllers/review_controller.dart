import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:nike_app_clone/constants.dart';
import 'package:nike_app_clone/models/comment.dart';

import '../models/user.dart';

class ReviewController extends GetxController {
  RxList<Comment> _comments = <Comment>[].obs;

  List<Comment> get comments => _comments.value;

  void clearComments() {
    _comments.clear();
  }

  void saveCommentToFirestore(
      String productId, int rating, String comment) async {
    try {
      final productDoc =
          FirebaseFirestore.instance.collection('products').doc(productId);

      await productDoc.collection('reviews').add({
        'rating': rating,
        'comment': comment,
        'timestamp': FieldValue.serverTimestamp(), 
        'userId': authController.user.uid
      });
      
      getComments(productId);
    } catch (e) {
      print("Error saving rating and comment: $e");
    }
  }

  int getTotalReviews(List<Comment> comments) {
    return comments.length;
  }

  Future<void> getComments(String productId) async {
    try {
      final productDoc =
          FirebaseFirestore.instance.collection('products').doc(productId);

      final commentsSnapshot = await productDoc
          .collection('reviews')
          .orderBy('timestamp', descending: true) 
          .get();

      if (commentsSnapshot.size > 0) {
        final List<Comment> commentList = [];
        commentsSnapshot.docs.forEach((commentDoc) {
          final commentData = commentDoc.data();
          final rating = commentData['rating'];
          final comment = commentData['comment'];
          final timestamp = commentData['timestamp'].toDate();
          final userId = commentData['userId'];

          final commentObj = Comment(
            rating: rating,
            content: comment,
            timestamp: timestamp,
            userId: userId,
          );
          commentList.add(commentObj);
        });

        _comments.value = commentList;
      } else {
        print("No comments available for this product.");
      }
    } catch (e) {
      print("Error getting comments: $e");
    }
  }

  Future<Map<String, dynamic>> getUserData(String userId) async {
    DocumentSnapshot userDoc =
        await FirebaseFirestore.instance.collection('users').doc(userId).get();
    if (userDoc.exists) {
      User user = User.fromSnap(userDoc);
      return user.toJson();
    } else {
      print("User does not exist");
      return {};
    }
  }
}
