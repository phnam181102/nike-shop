import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:nike_app_clone/constants.dart';

class ProfileController extends GetxController {
  final Rx<Map<String, dynamic>> _user = Rx<Map<String, dynamic>>({});
  final Rx<File?> _pickedImage = Rx<File?>(null);

  Map<String, dynamic> get user => _user.value;

  File? get pickedImage => _pickedImage.value;

  Rx<String> _uid = "".obs;

  void onReady() {
    resetUserData();
    getUserData();
    super.onReady();
  }

  void resetUserData() {
    _user.value = {};
    _pickedImage.value = null;
    update();
  }

  void getUserData() async {
    _uid.value = firebaseAuth.currentUser!.uid;
    DocumentSnapshot userDoc =
        await firestore.collection('users').doc(_uid.value).get();
    if (userDoc.exists) {
      final userData = userDoc.data()! as dynamic;
      String firstName = userData['firstName'];
      String lastName = userData['lastName'];
      String email = userData['email'];
      String phoneNumber = userData['phoneNumber'];
      String address = userData['address'];
      String profilePhoto = userData['profilePhoto'];
      DateTime createdDate = userData['createdDate'].toDate();
      String formattedDate = DateFormat('MMMM yyyy').format(createdDate);

      _user.value = {
        'profilePhoto': profilePhoto,
        'firstName': firstName,
        'lastName': lastName,
        'email': email,
        'phoneNumber': phoneNumber,
        'address': address,
        'createdDate': formattedDate,
      };
    } else {
      resetUserData();
    }
    update();
  }

  void updateProfile({
    required String firstName,
    required String lastName,
    required String email,
    required String phoneNumber,
    required String address,
  }) async {
    await FirebaseFirestore.instance
        .collection('users')
        .doc(_uid.value)
        .update({
      'firstName': firstName,
      'lastName': lastName,
      'email': email,
      'phoneNumber': phoneNumber,
      'address': address,
    });
    if (pickedImage != null) {
      saveImage(_pickedImage.value!); // Sử dụng await để chờ quá trình lưu ảnh
    }

    _user.update((userData) {
      userData!['firstName'] = firstName;
      userData['lastName'] = lastName;
      userData['email'] = email;
      userData['phoneNumber'] = phoneNumber;
      userData['address'] = address;
    });

    getUserData();
    Get.snackbar(
      'Success',
      'Amazing! Profile updated Successfully!',
      icon: SvgPicture.asset("assets/icons/success.svg"),
      margin: EdgeInsets.all(15),
      snackPosition: SnackPosition.TOP,
    );
  }

  Future<String> _uploadToStorage(File image) async {
    Reference ref = firebaseStorage
        .ref()
        .child('profilePics')
        .child(firebaseAuth.currentUser!.uid);

    UploadTask uploadTask = ref.putFile(image);
    TaskSnapshot snap = await uploadTask;
    String downloadUrl = await snap.ref.getDownloadURL();
    return downloadUrl;
  }

  void pickImage() async {
    final pickedImage =
        await ImagePicker().pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      _pickedImage.value = File(pickedImage.path);
    }
  }

  void saveImage(File image) async {
    if (_uid.value == firebaseAuth.currentUser!.uid) {
      String downloadUrl = await _uploadToStorage(image);
      await firestore
          .collection('users')
          .doc(_uid.value)
          .update({'profilePhoto': downloadUrl});

      _pickedImage.value = null;
      _user.update((userData) {
        userData!['profilePhoto'] = downloadUrl;
      });

      // Get.snackbar(
      //   'Success',
      //   'Amazing! Your image has been Uploaded Successfully!',
      //   icon: SvgPicture.asset("assets/icons/success.svg"),
      //   margin: EdgeInsets.all(15),
      //   snackPosition: SnackPosition.TOP,
      // );
    }
  }
}
